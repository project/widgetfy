<?php

// vim: set filetype=php expandtab tabstop=2 shiftwidth=2 autoindent smartindent:
namespace Drupal\widgetfy\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Phata\Widgetfy\Core as Widgetfy;

/**
 * @Filter(
 *   id = "filter_widgetfy",
 *   title = @Translation("Widgetfy Filter"),
 *   description = @Translation("Help to translate a-tags into embed video"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 *
 */
class FilterWidgetfy extends FilterBase {

  public function process($text, $langcode) {
    return new FilterProcessResult(self::replaceLinks($text));
  }

  /**
   * 
   */
  public static function replaceLinks($html) {
    $html_step1 = preg_replace_callback(
      '/<a .*?href="(.+?)".*?>(.*?)<\/a>/',
      array(self, 'replaceCallback'),
      $html
    );
    $html_step2 = preg_replace_callback(
      '/<a .*?href=\'(.+?)\'.*?>(.*?)<\/a>/',
      array(self, 'replaceCallback'),
      $html_step1
    );
    return $html_step2;
  }

  /**
   * preg_replace_callback callback function
   */
  public static function replaceCallback($matches) {
    $options = array(
      'width' => 1200,
    );
    if (($embed = Widgetfy::translate($matches[1], $options)) != NULL) {
      return FilterWidgetfy::template($embed);
    }
    return $matches[0];
  }

  /**
   * Helper function to render widgetfy template result
   */
  private static function template($embed) {
    return sprintf('<div %s><div %s>%s</div></div>',
      self::renderBlockAttrs($embed),
      self::renderWrapperAttrs($embed),
      $embed['html']);
  }

  /**
   * Helper funcion to template. Render attributes for .videoblock
   *
   * @param mixed[] $embed embed definition
   * @return string HTTP attributes
   */
  private static function renderBlockAttrs($embed) {
    // attributes to be rendered
    $classes = array();
    $styles = array();
    // shortcuts
    $d = &$embed['dimension'];
    // determine classes
    $classes[] = 'videoblock';
    if ($d->dynamic) {
      $classes[] = 'videoblock-dynamic';
    }
    // determine inline CSS style(s)
    // if scale model is no-scale, allow to "force dynamic"
    // by setting "dynamic" to TRUE
    if ($d->dynamic) {
      $styles[] = 'max-width:'.$d->width.'px';
    } else {
      $styles[] = 'width: '.$d->width.'px';
    }
    // render the attributes
    $class = implode(' ', $classes);
    $style = implode('; ', $styles) . (!empty($styles) ? ';' : '');
    return 'class="'.$class.'" style="'.$style.'"';
  }

  /**
   * Helper funcion to template. Render attributes for .videowrapper
   *
   * @param mixed[] $embed embed definition
   * @return string HTTP attributes
   */
  private static function renderWrapperAttrs($embed) {
    // attributes to be rendered
    $classes = array();
    $styles = array();
    // shortcuts
    $d = &$embed['dimension'];
    // determine classes
    $classes[] = 'video-wrapper';
    if ($d->dynamic) {
      $classes[] = 'wrap-'.$d->scale_model;
    }
    // determine inline CSS style(s)
    if ($d->dynamic && ($d->scale_model == 'scale-width-height')) {
      $styles[] = 'padding-bottom: ' . ($d->factor * 100) . '%;';
    }
    // render the attributes
    $class = implode(' ', $classes);
    $style = implode('; ', $styles) . (!empty($styles) ? ';' : '');
    return 'class="'.$class.'" style="'.$style.'"';
  }
}
